import allel
import h5py
import argparse
import glob
import os
import numpy as np
import pandas as pd
import time

HDF5_USE_FILE_LOCKING = 'FALSE'

# parametres
parser = argparse.ArgumentParser(description="append vcf(s) from ACGT project to hdf5")
parser.add_argument('--directory', help='directory with VCF files', required = True)
parser.add_argument('--hdf5', help='hdf5 file to append variant data', required = True)
parser.add_argument('--metadata_table', help='acgt metadata table (id,sex,region)', required = True)
args = parser.parse_args()
directory = args.directory
h5 = args.hdf5
metadata = args.metadata_table

chrom_l = list(['chr1','chr2','chr3','chr4','chr5','chr6','chr7','chr8','chr9','chr10','chr11','chr12','chr13','chr14','chr15','chr16','chr17','chr18','chr19','chr20','chr21','chr22','chrX','chrY'])

def append_to_hdf5(dir, h5_d):
    # string data type for hdf5
    dt = h5py.string_dtype(encoding='utf-8')

    met_tab = pd.read_csv(metadata, sep='\t', header=0)

    counter = 0
    vcfs = list(glob.iglob(dir + '/*_VEP.ann.NORM.vcf'))
    for vcf in vcfs:
        start = time.time()
        # open and parse vcf
        print("(1) Loading: " + vcf)
        vcf_file = allel.read_vcf(vcf,fields=['CHROM', 'POS', 'ALT', 'REF', 'GT','CSQ'])
        id = os.path.basename(vcf).replace('_VEP.ann.NORM.vcf','').replace('HaplotypeCaller_','')
        # create new sample record
        print("(2) Add variants record for: " + id)
        g = h5_d.create_group(id)
        # create metadata records !!! probably use g.attrs['sex'] = met_tab[met_tab['id'] == id]['sex']
        g.attrs['sex'] = met_tab[met_tab['id'] == id]['sex'].to_string(index=False)
        g.attrs['region'] = met_tab[met_tab['id'] == id]['region'].to_string(index=False)
        for chr in chrom_l:
            sg = g.create_group(chr)
            # get range of indexes in array for each chromosome
            chr_indexes = np.where(vcf_file['variants/CHROM'][:] == chr)
            p_min = np.amin(chr_indexes)
            p_max = np.amax(chr_indexes)
            #sssg1.create_dataset('CHROM', data = vcf_file['variants/CHROM'][p_min:p_max+1], dtype = dt)
            sg.create_dataset('POS', data = vcf_file['variants/POS'][p_min:p_max+1])
            sg.create_dataset('VAR', data = vcf_file['variants/POS'][p_min:p_max+1].astype(str) + vcf_file['variants/ALT'][p_min:p_max+1][:,0], dtype = dt)
            sg.create_dataset('REF', data = vcf_file['variants/REF'][p_min:p_max+1], dtype = dt)
            sg.create_dataset('ALT', data = vcf_file['variants/ALT'][p_min:p_max+1][:,0], dtype = dt)
            sg.create_dataset('GT', data = vcf_file['calldata/GT'][p_min:p_max+1])
            sg.create_dataset('ANNOT', data = vcf_file['variants/CSQ'][p_min:p_max+1], dtype = dt)
        counter +=1
        print(str(counter) + "/" + str(len(vcfs)))
        end = time.time()
        print(end-start)
    return dir

# open hdf5 file if existing, otherwise create

if os.path.exists(h5) == True:
    print("HDF5 file: " + h5 + " exists, loading.." )
    #h5_file = h5py.File(hdf5, "r+")
    with h5py.File(h5, "r+") as h5_data:
        append_to_hdf5(directory, h5_data)
else:
    print("HDF5 file: " + h5 + " not exist, creating new.." )
    #h5_file = h5py.File(hdf5, "a")
    with h5py.File(h5, "a") as h5_data:
        append_to_hdf5(directory, h5_data)
