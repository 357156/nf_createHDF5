import argparse
import h5py
import psycopg2
import sqlalchemy 

from sqlalchemy import create_engine
from sqlalchemy import String, Integer, Float, Text
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column

from sqlalchemy_utils import database_exists, create_database

import numpy as np


# | PHA |  Hlavní město Praha |
# | JHC |  Jihočeský |
# | JHM |  Jihomoravský |
# | KVK |  Karlovarský | 
# | VYS |  Vysočina | 
# | HKK |  Královehradecký | 
# | LBK |  Liberecký | 
# | MSK |  Moravskoslezský | 
# | OLK |  Olomoucký | 
# | PAK |  Pardubický | 
# | PLK |  Plzeňský | 
# | STC |  Středočeský | 
# | ULK |  Ústecký | 
# | ZLK |  Zlínský |
# | RUZ |  Různý | 
# | SVK |  Slovensko | 
# | NA  | Neznámý | 


class Base(DeclarativeBase):
    pass

class Variants(Base):
    __tablename__ = 'variants'
    id: Mapped[int] = mapped_column(primary_key=True)
    chrom: Mapped[str] = mapped_column(String(10))
    pos: Mapped[int] = mapped_column(Integer)
    ref: Mapped[str] = mapped_column(String(30))
    alt: Mapped[str] = mapped_column(String(40))
    annot: Mapped[str] = mapped_column(Text)
    sex_M_count: Mapped[int] = mapped_column(Integer)
    sex_M_freq: Mapped[float] = mapped_column(Float)
    sex_F_count: Mapped[int] = mapped_column(Integer)
    sex_F_freq: Mapped[float] = mapped_column(Float)
    sex_NA_count: Mapped[int] = mapped_column(Integer)
    sex_NA_freq: Mapped[float] = mapped_column(Float)
    freq_all: Mapped[float] = mapped_column(Float)
    count_all: Mapped[int] = mapped_column(Integer)
    freq_PHA: Mapped[float] = mapped_column(Float)
    count_PHA: Mapped[int] = mapped_column(Integer)
    freq_JHC: Mapped[float] = mapped_column(Float)
    count_JHC: Mapped[int] = mapped_column(Integer)
    freq_JHM: Mapped[float] = mapped_column(Float)
    count_JHM: Mapped[int] = mapped_column(Integer)
    freq_KVK: Mapped[float] = mapped_column(Float)
    count_KVK: Mapped[int] = mapped_column(Integer)
    freq_VYS: Mapped[float] = mapped_column(Float)
    count_VYS: Mapped[int] = mapped_column(Integer)
    freq_HKK: Mapped[float] = mapped_column(Float)
    count_HKK: Mapped[int] = mapped_column(Integer)
    freq_LBK: Mapped[float] = mapped_column(Float)
    count_LBK: Mapped[int] = mapped_column(Integer)
    freq_MSK: Mapped[float] = mapped_column(Float)
    count_MSK: Mapped[int] = mapped_column(Integer)
    freq_OLK: Mapped[float] = mapped_column(Float)
    count_OLK: Mapped[int] = mapped_column(Integer)
    freq_PAK: Mapped[float] = mapped_column(Float)
    count_PAK: Mapped[int] = mapped_column(Integer)
    freq_PLK: Mapped[float] = mapped_column(Float)
    count_PLK: Mapped[int] = mapped_column(Integer)
    freq_STC: Mapped[float] = mapped_column(Float)
    count_STC: Mapped[int] = mapped_column(Integer)
    freq_ULK: Mapped[float] = mapped_column(Float)
    count_ULK: Mapped[int] = mapped_column(Integer)
    freq_ZLK: Mapped[float] = mapped_column(Float)
    count_ZLK: Mapped[int] = mapped_column(Integer)
    freq_RUZ: Mapped[float] = mapped_column(Float)
    count_RUZ: Mapped[int] = mapped_column(Integer)
    freq_SVK: Mapped[float] = mapped_column(Float)
    count_SVK: Mapped[int] = mapped_column(Integer)
    freq_NA: Mapped[float] = mapped_column(Float)
    count_NA: Mapped[int] = mapped_column(Integer)
    count_samples: Mapped[int] = mapped_column(Integer)
    total_samples: Mapped[int] = mapped_column(Integer)
    sex_M_samples: Mapped[int] = mapped_column(Integer)
    sex_F_samples: Mapped[int] = mapped_column(Integer)
    sex_M_samples_freq: Mapped[float] = mapped_column(Float)
    sex_F_samples_freq: Mapped[float] = mapped_column(Float)
    samples_PHA: Mapped[int] = mapped_column(Integer)
    samples_all_PHA: Mapped[int] = mapped_column(Integer)
    samples_JHC: Mapped[int] = mapped_column(Integer)
    samples_all_JHC: Mapped[int] = mapped_column(Integer)
    samples_JHM: Mapped[int] = mapped_column(Integer)
    samples_all_JHM: Mapped[int] = mapped_column(Integer)
    samples_KVK: Mapped[int] = mapped_column(Integer)
    samples_all_KVK: Mapped[int] = mapped_column(Integer)
    samples_VYS: Mapped[int] = mapped_column(Integer)
    samples_all_VYS: Mapped[int] = mapped_column(Integer)
    samples_HKK: Mapped[int] = mapped_column(Integer)
    samples_all_HKK: Mapped[int] = mapped_column(Integer)
    samples_LBK: Mapped[int] = mapped_column(Integer)
    samples_all_LBK: Mapped[int] = mapped_column(Integer)
    samples_MSK: Mapped[int] = mapped_column(Integer)
    samples_all_MSK: Mapped[int] = mapped_column(Integer)
    samples_OLK: Mapped[int] = mapped_column(Integer)
    samples_all_OLK: Mapped[int] = mapped_column(Integer)
    samples_PAK: Mapped[int] = mapped_column(Integer)
    samples_all_PAK: Mapped[int] = mapped_column(Integer)
    samples_PLK: Mapped[int] = mapped_column(Integer)
    samples_all_PLK: Mapped[int] = mapped_column(Integer)
    samples_STC: Mapped[int] = mapped_column(Integer)
    samples_all_STC: Mapped[int] = mapped_column(Integer)
    samples_ULK: Mapped[int] = mapped_column(Integer)
    samples_all_ULK: Mapped[int] = mapped_column(Integer)
    samples_ZLK: Mapped[int] = mapped_column(Integer)
    samples_all_ZLK: Mapped[int] = mapped_column(Integer)
    samples_RUZ: Mapped[int] = mapped_column(Integer)
    samples_all_RUZ: Mapped[int] = mapped_column(Integer)
    samples_SVK: Mapped[int] = mapped_column(Integer)
    samples_all_SVK: Mapped[int] = mapped_column(Integer)
    samples_NA: Mapped[int] = mapped_column(Integer)
    samples_all_NA: Mapped[int] = mapped_column(Integer)


    def __repr__(self) -> str:
        return f"Variant(id={self.id}, chrom={self.chrom}, pos={self.pos}, ref={self.ref}, alt={self.alt}, annot={self.annot}, freq_all={self.freq_all}, count_all={self.count_all})"


def main():
    regions = "PHA JHC JHM KVK VYS HKK LBK MSK OLK PAK PLK STC ULK ZLK RUZ SVK NA".split()
    sexes = "M F NA".split()

    #user argparser to read in the path to the h5 file
    parser = argparse.ArgumentParser()
    parser.add_argument('-5', '--h5_path', help="Path to the h5 file", required=True)
    args = parser.parse_args()

    #open the h5 file
    h5_file = h5py.File(args.h5_path, 'r')
    total_samples = h5_file.attrs['Total_sample']
    reg = h5_file.attrs['Regional']

    region_counts_all = dict([ (l.split(":")[0], int(l.split(":")[1])) for l in h5_file.attrs['Regional']])

    
    #connect to the database
    engine = create_engine('postgresql+psycopg2://kubah:FQlsRs4rSYlSvlkknqawdZ1QSRsk2VuZgpAOCI7YUYpia8ts4r8arC2ekb97ceBK@acgt-db-deployment-rw/acgt-variants') 
    if not database_exists(engine.url):
        print("Creating database")
        create_database(engine.url)

    if not sqlalchemy.inspect(engine).has_table('variants'):
        Base.metadata.create_all(engine)
        print("Creating table")

    # iterate over the h5 file and insert the data into the database
    with engine.connect() as con:

        for var in h5_file.keys():

            # [chrom, alt, annot, gt, ref, region, samples, sex]

            current_var = h5_file[var]
            alts = current_var['alt'][:]
            unique_alts = set([a.decode() for a in alts])
            allele_count = (current_var['gt'][:].flatten() >= 1).sum() # calculate for each allele separately

            for ua in unique_alts:
                region_counts = dict([(x,0) for x in regions])
                region_samples = dict([(x,0) for x in regions])
                sex_counts = dict([(x,0) for x in sexes])
                sex_samples = dict([(x,0) for x in sexes])

                if ua == '':
                    continue

                # count the number of variants in each region
                for  idx, x in enumerate(alts):
                    # if current_var.name == '/chr1:100676887':
                    #import pdb
                    #pdb.set_trace()
                    if ua in x.decode():
                        reg = current_var['region'][idx].strip().decode()
                        if reg == 'NaN':
                            reg = 'NA'
                        if reg == 'Ruzny':
                            reg = 'RUZ'
                        if reg == 'Series([], )':
                            reg = 'NA'
                        region_counts[reg] += int((current_var['gt'][idx] >= 1).sum()) #1
                        region_samples[reg] += 1

                        sex = current_var['sex'][idx].strip().decode()
                        if sex == 'Series([], )':
                            sex = 'NA'
                        sex_counts[sex] += int((current_var['gt'][idx] >= 1).sum())
                        sex_samples[sex] += 1
                size = len(str(current_var['annot'][:][0]))
                ua_idx = []
                for idx, x in enumerate(alts):
                    if x.decode() == ua:
                        ua_idx.append(idx)
                current_allele_count = (current_var['gt'][ua_idx].flatten() >= 1).sum()

                annotation = []
                if(len(np.unique(current_var['annot'][:])) > 1):
                    if len(ua[:40]) > 1 or len(current_var['ref'][()].decode()[:30]) > 1:
                        annotation = list(filter(lambda x:'indel' or 'deletion' or 'insertion' in x.decode(), current_var['annot'][()]))
                    else:
                        annotation = list(filter(lambda x:'SNV' in x.decode(), current_var['annot'][()]))
                else:
                    annotation = current_var['annot'][()]


                if len(annotation) == 0 :
                    annotation = current_var['annot'][()]

                annotation = annotation[0].decode()

                #if str(current_var.attrs['pos'][0][0]) == '10071554' :
                #import pdb
                #pdb.set_trace()

                data = {
                    'chrom': current_var['chrom'][()].decode(),
                    'pos': str(current_var['pos'][:][0]),
                    'ref': current_var['ref'][()].decode()[:30],
                    'alt': ua[:40],
                    'annot': str(annotation),
                    'freq_all':  float(current_allele_count/(total_samples * 2)), #len(current_var.attrs['samples'])
                    'count_all': int(current_allele_count),
                    'count_samples': int(len(current_var['samples'][()])),
                    'total_samples': int(total_samples),
                }

                #iterate over the regions and add the data
                for region in regions:
                    count = region_counts[region]
                    data[f'count_{region}'] = count
                    data[f'samples_{region}'] = region_samples[region]
                    if count == 0:
                        data[f'freq_{region}'] = 0
                    else:
                        if region == 'NA':
                            data[f'freq_{region}'] = -1
                        else:
                            data[f'freq_{region}'] = count/(region_counts_all[region] * 2)
                            
                    if region not in region_counts_all:
                        data[f'samples_all_{region}'] = 0
                    else:
                        data[f'samples_all_{region}'] = region_counts_all[region]

                    #if region == 'PHA':
                    #    import pdb
                    #    pdb.set_trace()

                for x in sexes:
                    count = sex_counts[x]
                    count_samples = sex_samples[x]
                    data[f'sex_{x}_count'] = count
                    data[f'sex_{x}_samples'] = count_samples
                    data[f'sex_{x}_freq'] = count/(len(current_var['samples']) * 2)
                    data[f'sex_{x}_samples_freq'] = count_samples/(len(current_var['samples']))


                con.execute(Variants.__table__.insert(), data)
        
        con.commit()




if __name__ == "__main__":
    main()

