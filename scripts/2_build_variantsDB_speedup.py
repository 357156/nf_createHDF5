import h5py
import numpy as np
#from collections import defaultdict
import argparse
import time
import pandas as pd
from itertools import chain

parser = argparse.ArgumentParser(description="retrieve infos for each variant occuring in ACGT database")
parser.add_argument('--hdf5_file', help='path to hdf5 file', required = True)
parser.add_argument('--hdf5_var', help='path to new (final, for app) hdf5 file', required = True)
parser.add_argument('--chr', help='check chromosome', required = True)
parser.add_argument('--metadata_table', help='acgt metadata table (id,sex,region', required = True)
args = parser.parse_args()

hdf5 = args.hdf5_file
hdf5_var = args.hdf5_var
chrom = args.chr
meta = args.metadata_table

# get for chromosome all variants only once across all samples
def variant_record(h5, chr):
    z = []
    for sample in h5.keys():
        # set of numpy arrays
        z.append(h5[sample][chr]['VAR'][:])
    v = np.unique(np.concatenate(z))
    return v

def var_info(sample, var, chr, sample_l, region_l, sex_l, alt_l, ref_l, gt_l, annot_l, pos_l):
    pos_idx = np.where(h5_d[sample][chr]['VAR'][:] == var)
    if len(pos_idx[0]) > 0:
        sample_l.append(sample)
        region_l.append(h5_d[sample].attrs['region'])
        sex_l.append(h5_d[sample].attrs['sex'])
        pos_l.append(h5_d[sample][chr]['POS'][pos_idx])
        alt_l.append(h5_d[sample][chr]['ALT'][pos_idx])
        ref_l.append(h5_d[sample][chr]['REF'][pos_idx])
        gt_l.append(h5_d[sample][chr]['GT'][pos_idx])
        annot_l.append(h5_d[sample][chr]['ANNOT'][pos_idx])

def create_hdf(var, h5_d, h5_final, chr):
    sample_l = []
    region_l = []
    sex_l = []
    pos_l = []
    alt_l = []
    ref_l = []
    annot_l = []
    gt_l = []
    for sample in h5_d.keys():
        var_info(sample, var, chr, sample_l, region_l, sex_l, alt_l, ref_l, gt_l, annot_l, pos_l)
    g = h5_final.create_group(var)
    g.attrs['chrom'] = chr
    g.attrs['pos'] = pos_l
    g.attrs['samples'] = sample_l
    g.attrs['region'] = region_l
    g.attrs['sex'] = sex_l
    g.attrs['alt'] = np.array(alt_l)
    g.attrs['ref'] = np.array(ref_l[0])
    g.attrs['gt'] = gt_l
    g.attrs['annot'] = np.array(annot_l)

with h5py.File(hdf5, "r") as h5_d, h5py.File(hdf5_var, "a") as h5_final:
    l_var = variant_record(h5_d, chrom)

    counter = 0
    num_var = str(len(l_var))
    for variant in l_var:
        create_hdf(variant, h5_d, h5_final, chrom)
        counter += 1
        print(variant, ": ", str(counter) + "/" + num_var)

    h5_final.attrs['Total_sample'] = len(h5_d.keys())

    # regional occurence
    met_tab = pd.read_csv(meta, sep='\t', header=0)

    def join_l(a,b):
        return str(a) + ":" + str(b)

    reg = met_tab[met_tab.id.isin(h5_d.keys())]['region'].value_counts().keys().tolist()
    cnt = met_tab[met_tab.id.isin(h5_d.keys())]['region'].value_counts().tolist()
    print(list(map(join_l, reg,cnt)))
    h5_final.attrs['Regional'] = list(map(join_l, reg,cnt))

print('Done')
