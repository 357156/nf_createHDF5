import h5py
import numpy as np
import argparse
import time
import os
import zlib
import pandas as pd

class ACGTHDF5Builder():
    
    cache_INDEXED_VARIANTS = []
    cache_VARIANTS = {}
    cache_CHR_SAMPLES = {}
    cache_SAMPLES = []
    
    unique_variants = []
    number_of_unique_variants = 0

    chromosome = ""
    
    def __init__(self, chromosome, input_file, output_file, metadata_file):
        self.input_file = input_file
        self.output_file = output_file
        self.chromosome = chromosome
        self.metadata_file = metadata_file
        
    
    def hash_variant(self, variant):
        return zlib.crc32(variant) & 0xffffffff

    def var_info(self, sample, var, sample_l, region_l, sex_l, alt_l, ref_l, gt_l, annot_l, pos_l, var_hash, index_of_sample):

        pos_idx = self.cache_VARIANTS[sample].get(var_hash, None)

        if pos_idx is not None:
            sample_l[index_of_sample] = sample
            region_l[index_of_sample] = self.cache_CHR_SAMPLES[sample]['region']
            sex_l[index_of_sample] = self.cache_CHR_SAMPLES[sample]['sex']
            pos_l[index_of_sample] = self.cache_CHR_SAMPLES[sample]['POS'][pos_idx]
            alt_l[index_of_sample] = self.cache_CHR_SAMPLES[sample]['ALT'][pos_idx]
            ref_l[index_of_sample] = self.cache_CHR_SAMPLES[sample]['REF'][pos_idx]
            gt_l[index_of_sample] = self.cache_CHR_SAMPLES[sample]['GT'][pos_idx]
            annot_l[index_of_sample] = self.cache_CHR_SAMPLES[sample]['ANNOT'][pos_idx]
            index_of_sample += 1
        return index_of_sample

    def create_hdf(self, var, h5_final):
        var_hash = self.hash_variant(var)

        sample_size = sum(map(lambda x: 1 if var_hash in x else 0, self.cache_VARIANTS.values()))

        sample_l = [None] * sample_size
        region_l = [None] * sample_size
        sex_l = [None] * sample_size
        pos_l = [None] * sample_size
        alt_l = [None] * sample_size
        ref_l = [None] * sample_size
        annot_l = [None] * sample_size
        gt_l = [None] * sample_size

        index_of_sample = 0
        for sample in self.cache_SAMPLES:
            index_of_sample = self.var_info(sample, var, sample_l, region_l, sex_l, alt_l, ref_l, gt_l, annot_l, pos_l, var_hash, index_of_sample)

        g = h5_final.create_group(var)
        g['chrom'] = self.chromosome
        g['pos'] = pos_l
        g['samples'] = sample_l
        g['region'] = region_l
        g['sex'] = sex_l
        g['alt'] = alt_l
        g['ref'] = ref_l[0]
        g['gt'] = gt_l
        g['annot'] = annot_l
        
    def generate_cache(self):
        i_variants = {}
        with h5py.File(self.input_file, "r") as f:
            self.cache_SAMPLES = list(f.keys())
            for sample in f.keys():
                for variant in f[sample][self.chromosome]["VAR"][:]:
                    var_hash = self.hash_variant(variant)
                    if not i_variants.get(var_hash):
                        i_variants[var_hash] = True
                        self.unique_variants.append(variant)

                self.cache_VARIANTS[sample] = {self.hash_variant(x):i for i,x in enumerate(f[sample][self.chromosome]["VAR"][:], start=0)}

                self.cache_CHR_SAMPLES[sample] = {
                    "region": f[sample].attrs['region'],
                    "sex": f[sample].attrs['sex'],
                    "POS": f[sample][self.chromosome]["POS"][:],
                    "ALT": f[sample][self.chromosome]["ALT"][:],
                    "REF": f[sample][self.chromosome]["REF"][:],
                    "GT": f[sample][self.chromosome]["GT"][:],
                    "ANNOT": f[sample][self.chromosome]["ANNOT"][:],
                }
            self.number_of_unique_variants = len(self.unique_variants)

    def add_metadata(self, database):     
        database.attrs['Total_sample'] = len(self.cache_SAMPLES)
        # regional occurence
        met_tab = pd.read_csv(self.metadata_file, sep='\t', header=0)
    
        def join_l(a,b):
            return str(a) + ":" + str(b)
    
        reg = met_tab[met_tab.id.isin(self.cache_SAMPLES)]['region'].value_counts().keys().tolist()
        cnt = met_tab[met_tab.id.isin(self.cache_SAMPLES)]['region'].value_counts().tolist()
        print(list(map(join_l, reg,cnt)))
        database.attrs['Regional'] = list(map(join_l, reg,cnt))
        
    def build(self):
        build_start_time = time.time()
        print(f'Starting to generate all required cache from file database: {self.input_file}. Starting time {build_start_time}')
        self.generate_cache()
        build_end_time = time.time()
        print(f'Build of cache finished. End time {build_end_time}. Total time: {build_end_time-build_start_time}')

        build_start_time = time.time()
        print(f'Starting to build database. Output is set as {self.output_file}. Starting time {build_start_time}')
        with h5py.File(self.output_file, "a", driver='sec2') as h5_final:
            counter = 0
            for variant in self.unique_variants:
                self.create_hdf(variant, h5_final)
                counter += 1
                if counter % 1000 == 0:
                    print('{:5.2f}% done'.format(counter/self.number_of_unique_variants*100), "(" + str(counter) + "/" + str(self.number_of_unique_variants) + ")", time.strftime("%H:%M:%S", time.gmtime(time.time() - build_start_time)))
            print('{:5.2f}% done'.format(counter/self.number_of_unique_variants*100), "(" + str(counter) + "/" + str(self.number_of_unique_variants) + ")", time.strftime("%H:%M:%S", time.gmtime(time.time() - build_start_time)))

            print("Final step, adding metadata")
            self.add_metadata(h5_final)
            print("Metadata added")

            h5_final.flush()
            h5_final.close()
        build_end_time = time.time()
        print(f'Finished building database. Filename: {self.output_file}. Total time: {time.strftime("%H:%M:%S", time.gmtime(build_end_time-build_start_time))}')


def main():
    parser = argparse.ArgumentParser(description="retrieve infos for each variant occuring in ACGT database")
    parser.add_argument('--input_hdf5', help='path to hdf5 file', required = True)
    parser.add_argument('--output_hdf5', help='path to new (final, for app) hdf5 file', required = True)
    parser.add_argument('--chr', help='check chromosome', required = True)
    parser.add_argument('--metadata_file', help='acgt metadata table (id,sex,region', required = True)
    args = parser.parse_args()

    builder = ACGTHDF5Builder(args.chr, args.input_hdf5, args.output_hdf5, args.metadata_file)
    builder.build()
       
    
if __name__ == "__main__":
    main()