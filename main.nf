// Create final hdf5 for APP with all variants occuring in each of ACGT sample dataset 

// --input
chroms = params.input

inputChan = Channel.empty()
chromosomes = file(chroms)
inputChan = extractFastq(chromosomes)

process CREATE_HDF5 {

	publishDir "${launchDir}/results/", mode: 'copy'

	memory 256.GB

	input:
	tuple val(chrom)

	output:
	path "*"

	script:
	h5=params.hdf5
	meta=params.metad
        
	"""
	python3 /opt/conda/envs/hdf5/bin/2_build_variantsDB_optim.py --input_hdf5 ${h5} --output_hdf5 ${chrom}_toAPP.hdf5 --chr ${chrom} --metadata_file ${meta}
	"""

}

process ADD_TO_POSTGRESQL {

	input:
	path(hdf5)

	script:
	"""
	python3 /opt/conda/envs/hdf5/bin/3_import_hd5_to_db.py --h5_path $hdf5
	"""

}

workflow {

	hdf = CREATE_HDF5(inputChan)
	ADD_TO_POSTGRESQL(hdf)

}


def extractFastq(tsvFile) {
    Channel.from(tsvFile)
        .splitCsv(sep: '\t')
        .map { row ->
            def chrom      = row[0]
            [chrom]
        }
}
