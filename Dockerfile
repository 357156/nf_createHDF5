FROM nfcore/base:1.14

# Install the conda environment
COPY hdf5.yml /
RUN conda env create --quiet -f /hdf5.yml && conda clean -a

# Add conda installation dir to PATH (instead of doing 'conda activate')
ENV PATH /opt/conda/envs/hdf5/bin:$PATH

# Copy skript to bin
COPY ./scripts/2_build_variantsDB_optim.py /opt/conda/envs/hdf5/bin/
COPY ./scripts/3_import_hd5_to_db.py /opt/conda/envs/hdf5/bin/
